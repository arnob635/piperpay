package com.piperpay.pip_pay;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;

import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;



public class checkHistory extends AppCompatActivity {
    RecyclerView recyclerView;
    PaidInvoiceAdapter adapter;
    List <InvoiceJson> invoiceList;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_check_history);

        recyclerView = findViewById(R.id.recyclerView);
        recyclerView.setHasFixedSize(true);

        recyclerView.setLayoutManager(new LinearLayoutManager(this));



        FirebaseUser currentFirebaseUser = FirebaseAuth.getInstance().getCurrentUser() ;
        //Toast.makeText(this, "" + currentFirebaseUser.getUid(), Toast.LENGTH_SHORT).show();

        // mUid.setText(currentFirebaseUser.getUid());
       String uId = currentFirebaseUser.getUid();




        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl("https://piperpay.pythonanywhere.com/api/")
                .addConverterFactory(GsonConverterFactory.create())
                .build();

        JsonPlaceHolderApi jsonPlaceHolderApi = retrofit.create(JsonPlaceHolderApi.class);
        Call<Api> call = jsonPlaceHolderApi.getPaidInvoices("tahmidzubairarnobfariha","json",uId);
        call.enqueue(new Callback<Api>() {
            @Override
            public void onResponse(Call<Api> call, Response<Api> response) {

                if(!response.isSuccessful()){
                    Toast.makeText(checkHistory.this,"code " +response.code(),Toast.LENGTH_SHORT).show();
                    return;
                }

                Toast.makeText(checkHistory.this,"OK 3000 ",Toast.LENGTH_SHORT).show();

                invoiceList = response.body().getInvoices();
                adapter = new PaidInvoiceAdapter(checkHistory.this,invoiceList);
                recyclerView.setAdapter(adapter);
               /* List<InvoiceJson> history = (List<InvoiceJson>) response.body().getInvoices();

                for(InvoiceJson invoices : history){
                    String content = " ";
                    content += "Merchant Name " + invoices.getMerchant()+ "\n";
                    content += " Amount " + invoices.getAmount()+ "\n";
                    content += "Created on " + invoices.getCreatedOn()+ "\n";

                    transactions.append(content);
                } */


            }

            @Override
            public void onFailure(Call<Api> call, Throwable t) {
                Toast.makeText(checkHistory.this," " + t.getMessage(),Toast.LENGTH_SHORT).show();

            }
        });

    }


    }


