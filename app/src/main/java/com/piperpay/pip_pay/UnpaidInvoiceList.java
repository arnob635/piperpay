package com.piperpay.pip_pay;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.widget.Toast;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;

import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class UnpaidInvoiceList extends AppCompatActivity {

    RecyclerView Unpaidview;
    UnpaidInvoiceAdapter adapter;
    List <UnpaidInvoice> unpaidInvoiceList;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_unpaid_invoice_list);

        Unpaidview = findViewById(R.id.Unpaidview);

        Unpaidview.setHasFixedSize(true);

        Unpaidview.setLayoutManager(new LinearLayoutManager(this));

        FirebaseUser currentFirebaseUser = FirebaseAuth.getInstance().getCurrentUser() ;
        //Toast.makeText(this, "" + currentFirebaseUser.getUid(), Toast.LENGTH_SHORT).show();

        // mUid.setText(currentFirebaseUser.getUid());
        String uId = currentFirebaseUser.getUid();




        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl("https://piperpay.pythonanywhere.com/api/")
                .addConverterFactory(GsonConverterFactory.create())
                .build();

        JsonPlaceHolderApi jsonPlaceHolderApi = retrofit.create(JsonPlaceHolderApi.class);
        Call<Api> call = jsonPlaceHolderApi.getUnpaidInvoice("tahmidzubairarnobfariha","json",uId);
        call.enqueue(new Callback<Api>() {

            @Override
            public void onResponse(Call<Api> call, Response<Api> response) {

                if(!response.isSuccessful()){
                    Toast.makeText(UnpaidInvoiceList.this,"code " +response.code(),Toast.LENGTH_SHORT).show();
                    return;
                }

                Toast.makeText(UnpaidInvoiceList.this,"OK 3000 ",Toast.LENGTH_SHORT).show();

                assert response.body() != null;
                unpaidInvoiceList = response.body().getUnpaidInvoice();
                adapter = new UnpaidInvoiceAdapter(UnpaidInvoiceList.this,unpaidInvoiceList);
                Unpaidview.setAdapter(adapter);
               /* List<InvoiceJson> history = (List<InvoiceJson>) response.body().getInvoices();

                for(InvoiceJson invoices : history){
                    String content = " ";
                    content += "Merchant Name " + invoices.getMerchant()+ "\n";
                    content += " Amount " + invoices.getAmount()+ "\n";
                    content += "Created on " + invoices.getCreatedOn()+ "\n";

                    transactions.append(content);
                } */


            }

            @Override
            public void onFailure(Call<Api> call, Throwable t) {
                Toast.makeText(UnpaidInvoiceList.this," " + t.getMessage(),Toast.LENGTH_SHORT).show();

            }
        });
    }
}
