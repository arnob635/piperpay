package com.piperpay.pip_pay;

import android.content.Context;
import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.List;

public class UnpaidInvoiceAdapter extends RecyclerView.Adapter<UnpaidInvoiceAdapter.UnpaidInvoiceViewHolder> {

    private Context mCtx;
    private List<UnpaidInvoice> unpaidInvoiceList;

    public UnpaidInvoiceAdapter(Context mCtx, List<UnpaidInvoice> unpaidInvoiceList) {
        this.mCtx = mCtx;
        this.unpaidInvoiceList = unpaidInvoiceList;
}

@NonNull
@Override
public UnpaidInvoiceAdapter.UnpaidInvoiceViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup,int i){
        LayoutInflater inflater= LayoutInflater.from(mCtx);
        View view = inflater.inflate(R.layout.unpaid_invoice_list,null);
        UnpaidInvoiceAdapter.UnpaidInvoiceViewHolder holder = new UnpaidInvoiceAdapter.UnpaidInvoiceViewHolder(view);
        return holder;

    }

    @Override
    public void onBindViewHolder(@NonNull UnpaidInvoiceAdapter.UnpaidInvoiceViewHolder UnpaidInvoiceViewHolder, int i) {

        final UnpaidInvoice invoice = unpaidInvoiceList.get(i);


        UnpaidInvoiceViewHolder.tmerchantName.setText(new StringBuilder().append("Merchant Name : ").append(invoice.getMerchant()).toString());
        UnpaidInvoiceViewHolder.tamount.setText(new StringBuilder().append("Amount : ").append(invoice.getAmount()).toString());

        UnpaidInvoiceViewHolder.setItemClickListener(new ItemClickListener() {
            @Override
            public void onItemClick(View v, int position) {
                String merchant = invoice.getMerchant().toString();
                String username = invoice.getCustomer().toString();
                String status = invoice.getStatus().toString();
                String amount = invoice.getAmount().toString();
                String invoiceID = invoice.getId().toString();

                Intent intent = new Intent(mCtx,invoice.class);
                intent.putExtra("mMerchant",merchant);
                intent.putExtra("mUsername",username);
                intent.putExtra("mStatus",status);
                intent.putExtra("mAmount",amount);
                intent.putExtra("id",invoiceID);
                mCtx.startActivity(intent);
            }
        });


    }

    @Override
    public int getItemCount() {

            return unpaidInvoiceList.size();

    }

    class UnpaidInvoiceViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {

        ItemClickListener itemClickListener;
        TextView tmerchantName,tamount;

        public UnpaidInvoiceViewHolder(@NonNull View itemView) {
            super(itemView);


            tmerchantName=itemView.findViewById(R.id.merchantName);
            tamount=itemView.findViewById(R.id.amount);
            itemView.setOnClickListener(this);


        }

        @Override
        public void onClick(View v) {
            this.itemClickListener.onItemClick(v,getLayoutPosition());

        }

        public void setItemClickListener(ItemClickListener ic){
            this.itemClickListener = ic;
        }
    }


}
