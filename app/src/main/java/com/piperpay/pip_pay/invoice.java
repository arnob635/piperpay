package com.piperpay.pip_pay;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;

import java.util.ArrayList;
import java.util.List;

import io.branch.referral.Branch;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;


public class invoice extends AppCompatActivity {

    TextView mUserName,mMerchantName,mAmount,mStatus;
    ImageView mConfirm;
    String status,merchant,username,amount,id;




    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_invoice);
       // final TextView mpayments;
      // mpayments =  findViewById(R.id.payments);

        mUserName= findViewById(R.id.userName);
        mAmount= findViewById(R.id.amount);
        mMerchantName=findViewById(R.id.merchantName);
        mStatus=findViewById(R.id.status);
        mConfirm = (ImageView) findViewById(R.id.confirm);






        Intent intent = getIntent();
        merchant = intent.getStringExtra("mMerchant");
        amount = intent.getStringExtra("mAmount");
        username = intent.getStringExtra("mUsername");
        status = intent.getStringExtra("mStatus");
        id = intent.getStringExtra("id");

        Globals g = (Globals)getApplication();

        g.setInvoiceId(id);




        mMerchantName.setText(String.format("Merchant Name : %s",merchant));
        mAmount.setText(amount);
        mUserName.setText(String.format("Customer Name : %s",username));

        if(status.equals("0")) {
            mStatus.setText(String.format("Status : %s", "Unpaid"));
        }
        else
            mStatus.setText(String.format("Status : %s", "Paid"));

        mConfirm.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(invoice.this,fingerPrintLogin.class);
                startActivity(intent);
            }
        });



       /* FirebaseUser currentFirebaseUser = FirebaseAuth.getInstance().getCurrentUser() ;
        String uId = currentFirebaseUser.getUid();


        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl("https://piperpay.pythonanywhere.com/api/")
                .addConverterFactory(GsonConverterFactory.create())
                .build();

        JsonPlaceHolderApi jsonPlaceHolderApi = retrofit.create(JsonPlaceHolderApi.class);
        Call<Api> call = jsonPlaceHolderApi.getUnpaidInvoiceDetails("tahmidzubairarnobfariha","json",uId,invoiceId);
        call.enqueue(new Callback<Api>() {
            @Override
            public void onResponse(Call<Api> call, Response<Api> response) {

                if(!response.isSuccessful()){
                    Toast.makeText(invoice.this,"code " +response.code(),Toast.LENGTH_SHORT).show();
                    return;
                }

                mUserName.setText(String.format("Customer Name : %s", response.body().getInvoiceDetails().getCustomer()));
                mMerchantName.setText(String.format("Merchant Name : %s", response.body().getInvoiceDetails().getMerchant()));
                if(response.body().getInvoiceDetails().getStatus()==0){
                    status = "Unpaid";
                }
                mStatus.setText(String.format("Status : %s", status));
                mAmount.setText(response.body().getInvoiceDetails().getAmount());
                    mConfirm.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            Intent intent = new Intent(invoice.this,fingerPrintLogin.class);
                            startActivity(intent);
                        }
                    });

               /*List<UnpaidInvoice> invoiceList = response.body().getUnpaidInvoice();

                for(UnpaidInvoice invoices : payments){
                    String content = " ";
                    content += "Merchant Name " + invoices.getMerchant()+ "\n";
                    content += " Amount " + invoices.getAmount()+ "\n";
                    content += "Created on " + invoices.getCreatedOn()+ "\n";


                    mpayments.append(content);
                }



                Toast.makeText(invoice.this,"OK 3000 ",Toast.LENGTH_SHORT).show();




            }

            @Override
            public void onFailure(Call<Api> call, Throwable t) {
                Toast.makeText(invoice.this," " + t.getMessage(),Toast.LENGTH_SHORT).show();

            }
        });*/


    }
}
