package com.piperpay.pip_pay;

import android.content.Intent;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.CardView;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;


import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;


import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class dashboard extends AppCompatActivity {

     ImageView mLogoutBtn,mAlert;
     TextView mUserName,mBalamce;
     String uId;
     CardView mHistory,mPayment;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_dashboard);

        mLogoutBtn = (ImageView) findViewById(R.id.logoutBtn);
        FirebaseUser currentFirebaseUser = FirebaseAuth.getInstance().getCurrentUser() ;
        //Toast.makeText(this, "" + currentFirebaseUser.getUid(), Toast.LENGTH_SHORT).show();

       // mUid.setText(currentFirebaseUser.getUid());
        uId = currentFirebaseUser.getUid();

        mUserName = findViewById(R.id.userName);
        mBalamce = findViewById(R.id.balance);
        mHistory = (CardView) findViewById(R.id.history);
        mPayment = (CardView) findViewById(R.id.payment);
        mAlert = findViewById(R.id.alert);

        mAlert.setVisibility(View.INVISIBLE);

        mHistory.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(dashboard.this,checkHistory.class);
                startActivity(intent);
                return;
            }
        });

        mPayment.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(dashboard.this,UnpaidInvoiceList.class);
                startActivity(intent);
                return;
            }
        });



        mLogoutBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                FirebaseAuth.getInstance().signOut();
                Intent intent = new Intent(dashboard.this,Login.class);
                startActivity(intent);
                return;


            }
        });



        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl("https://piperpay.pythonanywhere.com/api/")
                .addConverterFactory(GsonConverterFactory.create())
                .build();

        JsonPlaceHolderApi jsonPlaceHolderApi = retrofit.create(JsonPlaceHolderApi.class);
        Call<Api> call = jsonPlaceHolderApi.getuserDetails("tahmidzubairarnobfariha","json",uId);
        call.enqueue(new Callback<Api>() {
            @Override
            public void onResponse(Call<Api> call, Response<Api> response) {

                if(!response.isSuccessful()){
                    Toast.makeText(dashboard.this,"code " +response.code(),Toast.LENGTH_SHORT).show();
                    return;
                }


                mUserName.setText(response.body().getUser().getUsername());
                mBalamce.setText(response.body().getUser().getAccountBalance());

                Toast.makeText(dashboard.this," "+response.body().getUser().getUsername(),Toast.LENGTH_SHORT).show();


            }

            @Override
            public void onFailure(Call<Api> call, Throwable t) {
                Toast.makeText(dashboard.this,"Loading Failed",Toast.LENGTH_SHORT).show();

            }
        });


        Call<Api> call2 = jsonPlaceHolderApi.getUnpaidInvoice("tahmidzubairarnobfariha","json",uId);
        call2.enqueue(new Callback<Api>() {
            @Override
            public void onResponse(Call<Api> call, Response<Api> response) {

                if(!response.isSuccessful()){
                    Toast.makeText(dashboard.this,"code " +response.code(),Toast.LENGTH_SHORT).show();
                    return;
                }

                //  mUserName.setText(response.body().getUnpaidInvoice().getCustomer());


                List<UnpaidInvoice> invoiceList = response.body().getUnpaidInvoice();

                String invoiceId = null;
                for(UnpaidInvoice invoices : invoiceList){


                    invoiceId = invoices.getId();

                }



                if(!invoiceList.isEmpty()) {
                    Globals g = (Globals) getApplication();
                    g.setInvoiceId(invoiceId);
                    mAlert.setVisibility(View.VISIBLE);

                }




                Toast.makeText(dashboard.this,"OK 3000 ",Toast.LENGTH_SHORT).show();




            }

            @Override
            public void onFailure(Call<Api> call, Throwable t) {
                Toast.makeText(dashboard.this," " + t.getMessage(),Toast.LENGTH_SHORT).show();

            }
        });





    }




}
