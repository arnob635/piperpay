package com.piperpay.pip_pay;

import android.view.View;

public interface ItemClickListener {

    void onItemClick(View v, int position);
}
