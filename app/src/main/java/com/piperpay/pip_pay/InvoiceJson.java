package com.piperpay.pip_pay;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class InvoiceJson {
    @SerializedName("id")
    @Expose
    private String id;
    @SerializedName("created_on")
    @Expose
    private String createdOn;
    @SerializedName("merchant")
    @Expose
    private String merchant;
    @SerializedName("public_information")
    @Expose
    private String publicInformation;
    @SerializedName("amount")
    @Expose
    private String amount;
    @SerializedName("status")
    @Expose
    private Integer status;
    @SerializedName("paid_on")
    @Expose
    private String paidOn;
    @SerializedName("customer")
    @Expose
    private String customer;
    @SerializedName("refunded_on")
    @Expose
    private Object refundedOn;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getCreatedOn() {
        return createdOn;
    }

    public void setCreatedOn(String createdOn) {
        this.createdOn = createdOn;
    }

    public String getMerchant() {
        return merchant;
    }

    public void setMerchant(String merchant) {
        this.merchant = merchant;
    }

    public String getPublicInformation() {
        return publicInformation;
    }

    public void setPublicInformation(String publicInformation) {
        this.publicInformation = publicInformation;
    }

    public String getAmount() {
        return amount;
    }

    public void setAmount(String amount) {
        this.amount = amount;
    }

    public Integer getStatus() {
        return status;
    }

    public void setStatus(Integer status) {
        this.status = status;
    }

    public String getPaidOn() {
        return paidOn;
    }

    public void setPaidOn(String paidOn) {
        this.paidOn = paidOn;
    }

    public String getCustomer() {
        return customer;
    }

    public void setCustomer(String customer) {
        this.customer = customer;
    }

    public Object getRefundedOn() {
        return refundedOn;
    }

    public void setRefundedOn(Object refundedOn) {
        this.refundedOn = refundedOn;
    }
}
