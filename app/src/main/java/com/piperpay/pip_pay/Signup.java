package com.piperpay.pip_pay;

import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.Editable;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class Signup extends AppCompatActivity {

    Button mBtn;
    EditText mEmail,mPass,cPass,mUserName,mFirstName,mLastName;
    String uId;

    private FirebaseAuth mAuth;
    private FirebaseAuth.AuthStateListener firebaseAuthListner;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_signup);

        mEmail =  findViewById(R.id.signUpEmail);
        mPass =  findViewById(R.id.signUpPass);
        cPass =  findViewById(R.id.confirmPass);
        mUserName = findViewById(R.id.userName);
        mFirstName = findViewById(R.id.firstName);
        mLastName = findViewById(R.id.lastName);

        mAuth = FirebaseAuth.getInstance();
        firebaseAuthListner = new FirebaseAuth.AuthStateListener() {
            @Override
            public void onAuthStateChanged(@NonNull FirebaseAuth firebaseAuth) {
                FirebaseUser user = FirebaseAuth.getInstance().getCurrentUser();
                if(user!=null){
                    Intent intent = new Intent(Signup.this,dashboard.class);
                    FirebaseUser currentFirebaseUser = FirebaseAuth.getInstance().getCurrentUser() ;

                    uId = currentFirebaseUser.getUid();
                    startActivity(intent);
                    finish();
                    return;
                }
            }
        };






        mBtn= findViewById(R.id.signupBtn);

        mBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                final String email = mEmail.getText().toString();
                final String password = mPass.getText().toString();
                final String confirmPass = cPass.getText().toString();
                if(!email.isEmpty()||!password.isEmpty()|| !confirmPass.isEmpty()) {
                    if (password.equals(confirmPass)) {
                        mAuth.createUserWithEmailAndPassword(email, password).addOnCompleteListener(Signup.this, new OnCompleteListener<AuthResult>() {
                            @Override
                            public void onComplete(@NonNull Task<AuthResult> task) {
                                if (!task.isSuccessful()) {
                                    Toast.makeText(Signup.this, "Signup Error", Toast.LENGTH_SHORT).show();
                                } else {
                                    String userId = mAuth.getCurrentUser().getUid();
                                    DatabaseReference currentUdb = FirebaseDatabase.getInstance().getReference().child("Users").child(userId);
                                    currentUdb.setValue(true);
                                    final String email = mEmail.getText().toString();
                                    final String first_name = mFirstName.getText().toString();
                                    final String last_name = mLastName.getText().toString();
                                    final String userName = mUserName.getText().toString();

                                    Retrofit retrofit = new Retrofit.Builder()
                                            .baseUrl("https://piperpay.pythonanywhere.com/api/")
                                            .addConverterFactory(GsonConverterFactory.create())
                                            .build();

                                    JsonPlaceHolderApi jsonPlaceHolderApi = retrofit.create(JsonPlaceHolderApi.class);
                                    Call<Api> call = jsonPlaceHolderApi.createUser("tahmidzubairarnobfariha",email,first_name,"json",uId,last_name,userName);
                                    call.enqueue(new Callback<Api>() {
                                        @Override
                                        public void onResponse(Call<Api> call, Response<Api> response) {

                                            if(!response.isSuccessful()){
                                                Toast.makeText(Signup.this,"code " +response.code(),Toast.LENGTH_SHORT).show();
                                                return;
                                            }



                                            Toast.makeText(Signup.this," "+response.body().getUser().getUsername(),Toast.LENGTH_SHORT).show();


                                        }

                                        @Override
                                        public void onFailure(Call<Api> call, Throwable t) {
                                            Toast.makeText(Signup.this,"Loading Failed",Toast.LENGTH_SHORT).show();

                                        }
                                    });



                                }
                            }
                        });
                    } else {
                        Toast.makeText(Signup.this, "Passwords do not match", Toast.LENGTH_SHORT).show();
                    }
                }else Toast.makeText(Signup.this, "SignUp Error", Toast.LENGTH_SHORT).show();

            }

        });


    }

    @Override
    protected void onStart(){
        super.onStart();
        mAuth.addAuthStateListener(firebaseAuthListner);


    }

    @Override
    protected void onStop(){
        super.onStop();
        mAuth.removeAuthStateListener(firebaseAuthListner);
    }

}
