package com.piperpay.pip_pay;

import android.app.Application;

public class Globals extends Application {

    public String invoiceId;

    public String getInvoiceId(){
        return invoiceId;
    }

    public void setInvoiceId(String i){
        this.invoiceId=i;
    }
}
