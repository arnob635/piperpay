package com.piperpay.pip_pay;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.TextView;
import android.widget.Toast;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class Payment extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_payment);

        Globals g = (Globals)getApplication();

        String invoiceId = g.getInvoiceId();

     final TextView txt = findViewById(R.id.textView2);
        FirebaseUser currentFirebaseUser = FirebaseAuth.getInstance().getCurrentUser() ;
        //Toast.makeText(this, "" + currentFirebaseUser.getUid(), Toast.LENGTH_SHORT).show();

        // mUid.setText(currentFirebaseUser.getUid());
      String uId = currentFirebaseUser.getUid();

        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl("https://piperpay.pythonanywhere.com/api/")
                .addConverterFactory(GsonConverterFactory.create())
                .build();

        JsonPlaceHolderApi jsonPlaceHolderApi = retrofit.create(JsonPlaceHolderApi.class);
        Call<Api> call = jsonPlaceHolderApi.payInvoice("tahmidzubairarnobfariha","json",uId,invoiceId);
        call.enqueue(new Callback<Api>() {
            @Override
            public void onResponse(Call<Api> call, Response<Api> response) {

                if(!response.isSuccessful()){
                    Toast.makeText(Payment.this,"code " +response.code(),Toast.LENGTH_SHORT).show();
                    txt.setText("Payment failed");
                    Intent myIntent = new Intent(Payment.this, dashboard.class);
                    startActivity(myIntent);
                    return;
                }


                Toast.makeText(Payment.this," " + response.body().getMessage(),Toast.LENGTH_SHORT).show();
                txt.setText("Payment Successful");
                Intent myIntent = new Intent(Payment.this, dashboard.class);
                startActivity(myIntent);

            }

            @Override
            public void onFailure(Call<Api> call, Throwable t) {
                Toast.makeText(Payment.this," " + t.getMessage(),Toast.LENGTH_SHORT).show();

            }
        });

    }
}
