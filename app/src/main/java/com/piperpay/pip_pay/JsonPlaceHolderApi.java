package com.piperpay.pip_pay;


import java.util.List;

import retrofit2.Call;

import retrofit2.http.GET;

import retrofit2.http.Query;



public interface JsonPlaceHolderApi {

    @GET("user-details")
    Call<Api> getuserDetails(
            @Query("api_key") String apiKey,
            @Query("format") String format,
            @Query("id") String id
    );

    @GET("user-paid-invoice-list")
    Call<Api>getPaidInvoices(
            @Query("api_key") String apiKey,
            @Query("format") String format,
            @Query("id") String id
    );

    @GET("user-unpaid-invoice-list")
    Call<Api>getUnpaidInvoice(
            @Query("api_key") String apiKey,
            @Query("format") String format,
            @Query("id") String id
    );

    @GET("user-invoice-details")
    Call<Api>getUnpaidInvoiceDetails(
            @Query("api_key") String apiKey,
            @Query("format") String format,
            @Query("id") String id,
            @Query("invoice_id") String invoiceId
    );

    @GET("user-pay-invoice")
    Call<Api>payInvoice(
            @Query("api_key") String apiKey,
            @Query("format") String format,
            @Query("id") String id,
            @Query("invoice_id") String invoiceId
    );

    @GET("create-user")
    Call<Api>createUser(
            @Query("api_key") String apiKey,
            @Query("email") String email,
            @Query("first_name") String firstName,
            @Query("format") String format,
            @Query("id") String id,
            @Query("last_name") String lastName,
            @Query("username") String userName
    );


}
