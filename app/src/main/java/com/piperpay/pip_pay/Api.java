package com.piperpay.pip_pay;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class Api  {

    @SerializedName("user")
    @Expose
     private userDetails user;

    public userDetails getUser() {
        return user;
    }

    @SerializedName("invoices")
    @Expose
    private List<InvoiceJson> invoices = null;

    public List<InvoiceJson> getInvoices() {
        return invoices;
    }

    @SerializedName("unpaid_invoices")
    @Expose
    private List<UnpaidInvoice> unpaidInvoice = null;
    public List<UnpaidInvoice> getUnpaidInvoice(){
        return unpaidInvoice;
    }

    @SerializedName("invoice")
    @Expose
    private InvoiceDetails invoice;

    public InvoiceDetails getInvoiceDetails() {
        return invoice;
    }

    @SerializedName("message")
    @Expose
    private String message;

    public String getMessage() {
        return message;
    }

}
