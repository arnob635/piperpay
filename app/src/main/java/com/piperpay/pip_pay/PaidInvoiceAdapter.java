package com.piperpay.pip_pay;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.List;

public class PaidInvoiceAdapter extends RecyclerView.Adapter<PaidInvoiceAdapter.PaidInvoiceViewHolder> {

    private Context mCtx;
    private List<InvoiceJson> invoiceList;

    public PaidInvoiceAdapter(Context mCtx, List<InvoiceJson> invoiceList) {
        this.mCtx = mCtx;
        this.invoiceList = invoiceList;
    }

    @NonNull
    @Override
    public PaidInvoiceAdapter.PaidInvoiceViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        LayoutInflater inflater= LayoutInflater.from(mCtx);
        View view = inflater.inflate(R.layout.paid_invoice_layout,null);
        PaidInvoiceAdapter.PaidInvoiceViewHolder holder = new PaidInvoiceAdapter.PaidInvoiceViewHolder(view);
        return holder;
    }

    @Override
    public void onBindViewHolder(@NonNull PaidInvoiceAdapter.PaidInvoiceViewHolder PaidInvoiceViewHolder, int i) {

        final InvoiceJson invoice = invoiceList.get(i);

        PaidInvoiceViewHolder.tDate.setText(new StringBuilder().append("Date Created : ").append(invoice.getCreatedOn()).toString());
        PaidInvoiceViewHolder.tMerchantName.setText(new StringBuilder().append("Merchant Name : ").append(invoice.getMerchant()).toString());
        PaidInvoiceViewHolder.tAmount.setText(new StringBuilder().append("Amount : ").append(invoice.getAmount()).toString());




    }

    @Override
    public int getItemCount() {
        return invoiceList.size();
    }

    class PaidInvoiceViewHolder extends RecyclerView.ViewHolder {

        TextView tDate,tMerchantName,tAmount;

        public PaidInvoiceViewHolder(@NonNull View itemView) {
            super(itemView);

            tDate=itemView.findViewById(R.id.date);
            tMerchantName=itemView.findViewById(R.id.merchantName);
            tAmount=itemView.findViewById(R.id.amount);


        }
    }


}
